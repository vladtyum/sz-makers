from django.db import models
from django.utils.text import slugify
from django.core.urlresolvers import reverse
from ckeditor_uploader.fields import RichTextUploadingField



class Letter(models.Model):
    title = models.CharField(max_length=150, null=True, blank=False)
    slug = models.SlugField(max_length=150, unique=True, verbose_name='slug', null=True, blank=False)
    date = models.DateField(null=True, blank=True)
    richcontent = RichTextUploadingField(null=True, blank=True)
    is_live = models.BooleanField(blank=True, default=False)

    class Meta:
        ordering = ['-date']
        verbose_name_plural = "Events"

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)
        super(Letter, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('letter-detail', kwargs={'slug': self.slug,
                                                 'pk': self.pk,})