Django==1.9.7
django-ckeditor==5.0.3
django-redis-cache==1.6.5
Pillow==3.2.0
psycopg2==2.6.2
redis==2.10.5
sorl-thumbnail==12.3
wheel==0.24.0
