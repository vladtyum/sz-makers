from django import forms
from django.contrib.auth.models import User
from .models import Org, Event
from django.contrib.admin.widgets import AdminDateWidget
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from ckeditor.widgets import CKEditorWidget

class OrgForm(forms.ModelForm):
    about_en = forms.CharField(
                    help_text='<i>short inro about organizer </i>', 
                    widget=forms.Textarea(attrs={'rows': 3}))
    about_cn = forms.CharField(
                    help_text='<i>项目介绍 </i>', 
                    widget=forms.Textarea(attrs={'rows': 3}))
    richcontent = forms.CharField(
                    widget=CKEditorWidget(attrs={'cols':50,'rows': 3}),
                    help_text="""^^^ more details about your project,
                    put pictures, tables,
                    files whatever you want for introducing your project"""
                    )

    class Meta:
        model = Org
        widgets = {'user': forms.HiddenInput()}
        exclude = ['slug']

    # user = models.ForeignKey(User, null=True, blank=True) 
    # orgname = models.CharField(max_length=150, null=False, blank=False)
    # slug = models.SlugField(max_length=150, unique=True, verbose_name='slug', null=True, blank=True)
    # logo = models.ImageField(upload_to='uploads/org/logos/', null=True, blank=True)
    # qr = models.ImageField(upload_to='uploads/org/qrs/', null=True, blank=True )
    # moto = models.CharField(max_length=200, null=True, blank=True)
    # location_cn = models.CharField(max_length=200, null=True, blank=True)
    # location_en = models.CharField(max_length=200, null=True, blank=True)


class EventForm(forms.ModelForm):
    #sectors
    start_date = forms.DateField(widget=forms.SelectDateWidget())
    start_time = forms.TimeField(
                    widget=forms.TextInput(attrs={'size': 5}),
                    initial="19:11",
                    help_text='<i> "hh:mm" format</i>',)
    fee = forms.DecimalField(
                    initial=None,
                    help_text='<i>if event is Free, paste 0 </i>',)
    content_en = forms.CharField(
                    help_text='<i>short info for notifications ans stuff </i>', 
                    widget=forms.Textarea(attrs={'rows': 3}))
    content_cn = forms.CharField(
                    help_text='<i>in CN , short info for notifications ans stuff </i>', 
                    widget=forms.Textarea(attrs={'rows': 3}))
    richcontent = forms.CharField(widget=CKEditorWidget(attrs={'cols':50,'rows': 3}))

    class Meta:
        model = Event
        widgets = {'author': forms.HiddenInput(),
                    'org': forms.HiddenInput(),
                    }
        
        fields = ("__all__")
    
    # i guess this block is not needed anymore, since AutoSlug installed
    # but leave it for now, just in a case
    # def clean_slug(self):
    #         data = self.cleaned_data['slug']
    #         if Event.objects.filter(slug=data).exists():
    #             raise ValidationError("this slug exists")
    #         return data


class EventUpdateForm(forms.ModelForm):
    #sectors
    start_date = forms.DateField(widget=forms.SelectDateWidget())
    start_time = forms.TimeField(
                    widget=forms.TextInput(attrs={'size': 5}),
                    initial="19:11",
                    help_text='<i> "hh:mm" format</i>',)
    fee = forms.DecimalField(
                    initial=None,
                    help_text='<i>if event is Free, paste 0 </i>',)
    content_en = forms.CharField(
                    help_text='<i>short info for notifications ans stuff </i>', 
                    widget=forms.Textarea(attrs={'rows': 3}))
    content_cn = forms.CharField(
                    help_text='<i>in CN , short info for notifications ans stuff </i>', 
                    widget=forms.Textarea(attrs={'rows': 3}))
    richcontent = forms.CharField(widget=CKEditorWidget(attrs={'cols':50,'rows': 3}))

    class Meta:
        model = Event
        widgets = {'author': forms.HiddenInput(),
                    'org': forms.HiddenInput(),
                    }
        
        fields = ("__all__")


class OrgUpdateForm(forms.ModelForm):
    orgname = forms.CharField(disabled=True)
    about_en = forms.CharField(help_text='<i>short info about organizer </i>')
    class Meta:
        model = Org
        exclude = ['user']
